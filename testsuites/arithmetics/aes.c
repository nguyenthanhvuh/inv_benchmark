#include <stdio.h>
#include <math.h>
#include <stdlib.h>  //required for afloat to work
#include <assert.h>
#include <time.h>
#include <string.h>
#include <ctype.h>
#include "inv_common.h"

/*aes common*/

const int XOR_OP = 0;
const int AND_OP = 1;

int xor(const int x, const int y){return x ^ y;}

int myop(const int xs[], const int N, const int opt){
  assert(N > 1);
  int i;
  int r = xs[0];
  for(i=1; i<N; ++i){
    if (opt == XOR_OP){
      r = r ^ xs[i];
    }
    else if(opt == AND_OP){
      r = r & xs[i];
    }
  }

  return r;
}

int myxor(int xs[], const int N){return myop(xs,N,XOR_OP);}
int myand(int xs[], const int N){return myop(xs,N,AND_OP);}


const int MAXKC = 8;
const int MAXNR = 14;

const int byte = 256;  //256

const int is_byte(const int b){return 0 <= b || b <= byte - 1;}

int check_byte(const int b[], const int N, const int N_){
  if  (N != N_){
    return 0;
  }
  int i = 0 ;
  for(i = 0; i < N; ++i){
    if (is_byte(b[i]) == 0){
      return 0;
    }
  }
  return 1;
}


const int byte_index = 16; 
int is_block(const int b[byte_index], const int N){
  return check_byte(b,N,byte_index);
}

const int key_index = 32;  
int is_key(const int b[], const int N){
  return check_byte(b,N,key_index);
}

const int roundkey_index = 60; 



/* aes refactored */

int xor_xor(const int x, const int y, const int z){return xor(xor(x,y),z);}
const int word_index = 4;
int is_word(const int b[], const int N){return check_byte(b,N,word_index);}
int is_byte_table(const int b[], const int N){return check_byte(b,N,byte);}

const int state_index = 4; //0..3
//is_state = lambda s: len(s) == state_index and all(is_word(w) for w in s)


int Logtable [256]= {
  0,   0,  25,   1,  50,   2,  26, 198,  75, 199,  27, 104,  51, 238, 223,   3,
  100,   4, 224,  14,  52, 141, 129, 239,  76, 113,   8, 200, 248, 105,  28, 193,
  125, 194,  29, 181, 249, 185,  39, 106,  77, 228, 166, 114, 154, 201,   9, 120,
  101,  47, 138,   5,  33,  15, 225,  36,  18, 240, 130,  69,  53, 147, 218, 142,
  150, 143, 219, 189,  54, 208, 206, 148,  19,  92, 210, 241,  64,  70, 131,  56,
  102, 221, 253,  48, 191,   6, 139,  98, 179,  37, 226, 152,  34, 136, 145,  16,
  126, 110,  72, 195, 163, 182,  30,  66,  58, 107,  40,  84, 250, 133,  61, 186,
  43, 121,  10,  21, 155, 159,  94, 202,  78, 212, 172, 229, 243, 115, 167,  87,
  175,  88, 168,  80, 244, 234, 214, 116,  79, 174, 233, 213, 231, 230, 173, 232,
  44, 215, 117, 122, 235,  22,  11, 245,  89, 203,  95, 176, 156, 169,  81, 160,
  127,  12, 246, 111,  23, 196,  73, 236, 216,  67,  31,  45, 164, 118, 123, 183,
  204, 187,  62,  90, 251,  96, 177, 134,  59,  82, 161, 108, 170,  85,  41, 157,
  151, 178, 135, 144,  97, 190, 220, 252, 188, 149, 207, 205,  55,  63,  91, 209,
  83,  57, 132,  60,  65, 162, 109,  71,  20,  42, 158,  93,  86, 242, 211, 171,
  68,  17, 146, 217,  35,  32,  46, 137, 180, 124, 184,  38, 119, 153, 227, 165,
  103,  74, 237, 222, 197,  49, 254,  24,  13,  99, 140, 128, 192, 247, 112,   7
};



int Alogtable[256] = {
  1,   3,   5,  15,  17,  51,  85, 255,  26,  46, 114, 150, 161, 248,  19,  53,
  95, 225,  56,  72, 216, 115, 149, 164, 247,   2,   6,  10,  30,  34, 102, 170,
  229,  52,  92, 228,  55,  89, 235,  38, 106, 190, 217, 112, 144, 171, 230,  49,
  83, 245,   4,  12,  20,  60,  68, 204,  79, 209, 104, 184, 211, 110, 178, 205,
  76, 212, 103, 169, 224,  59,  77, 215,  98, 166, 241,   8,  24,  40, 120, 136,
  131, 158, 185, 208, 107, 189, 220, 127, 129, 152, 179, 206,  73, 219, 118, 154,
  181, 196,  87, 249,  16,  48,  80, 240,  11,  29,  39, 105, 187, 214,  97, 163,
  254,  25,  43, 125, 135, 146, 173, 236,  47, 113, 147, 174, 233,  32,  96, 160,
  251,  22,  58,  78, 210, 109, 183, 194,  93, 231,  50,  86, 250,  21,  63,  65,
  195,  94, 226,  61,  71, 201,  64, 192,  91, 237,  44, 116, 156, 191, 218, 117,
  159, 186, 213, 100, 172, 239,  42, 126, 130, 157, 188, 223, 122, 142, 137, 128,
  155, 182, 193,  88, 232,  35, 101, 175, 234,  37, 111, 177, 200,  67, 197,  84,
  252,  31,  33,  99, 165, 244,   7,   9,  27,  45, 119, 153, 176, 203,  70, 202,
  69, 207,  74, 222, 121, 139, 134, 145, 168, 227,  62,  66, 198,  81, 243,  14,
  18,  54,  90, 238,  41, 123, 141, 140, 143, 138, 133, 148, 167, 242,  13,  23,
  57,  75, 221, 124, 132, 151, 162, 253,  28,  36, 108, 180, 199,  82, 246,   1
};



int S[256] = {
  99, 124, 119, 123, 242, 107, 111, 197,  48,   1, 103,  43, 254, 215, 171, 118,
  202, 130, 201, 125, 250,  89,  71, 240, 173, 212, 162, 175, 156, 164, 114, 192,
  183, 253, 147,  38,  54,  63, 247, 204,  52, 165, 229, 241, 113, 216,  49,  21,
  4, 199,  35, 195,  24, 150,   5, 154,   7,  18, 128, 226, 235,  39, 178, 117,
  9, 131,  44,  26,  27, 110,  90, 160,  82,  59, 214, 179,  41, 227,  47, 132,
  83, 209,   0, 237,  32, 252, 177,  91, 106, 203, 190,  57,  74,  76,  88, 207,
  208, 239, 170, 251,  67,  77,  51, 133,  69, 249,   2, 127,  80,  60, 159, 168,
  81, 163,  64, 143, 146, 157,  56, 245, 188, 182, 218,  33,  16, 255, 243, 210,
  205,  12,  19, 236,  95, 151,  68,  23, 196, 167, 126,  61, 100,  93,  25, 115,
  96, 129,  79, 220,  34,  42, 144, 136,  70, 238, 184,  20, 222,  94,  11, 219,
  224,  50,  58,  10,  73,   6,  36,  92, 194, 211, 172,  98, 145, 149, 228, 121,
  231, 200,  55, 109, 141, 213,  78, 169, 108,  86, 244, 234, 101, 122, 174,   8,
  186, 120,  37,  46,  28, 166, 180, 198, 232, 221, 116,  31,  75, 189, 139, 138,
  112,  62, 181, 102,  72,   3, 246,  14,  97,  53,  87, 185, 134, 193,  29, 158,
  225, 248, 152,  17, 105, 217, 142, 148, 155,  30, 135, 233, 206,  85,  40, 223,
  140, 161, 137,  13, 191, 230,  66, 104,  65, 153,  45,  15, 176,  84, 187,  22
};



int Si[256] = {
  82,   9, 106, 213,  48,  54, 165,  56, 191,  64, 163, 158, 129, 243, 215, 251,
  124, 227,  57, 130, 155,  47, 255, 135,  52, 142,  67,  68, 196, 222, 233, 203,
  84, 123, 148,  50, 166, 194,  35,  61, 238,  76, 149,  11,  66, 250, 195,  78,
  8,  46, 161, 102,  40, 217,  36, 178, 118,  91, 162,  73, 109, 139, 209,  37,
  114, 248, 246, 100, 134, 104, 152,  22, 212, 164,  92, 204,  93, 101, 182, 146,
  108, 112,  72,  80, 253, 237, 185, 218,  94,  21,  70,  87, 167, 141, 157, 132,
  144, 216, 171,   0, 140, 188, 211,  10, 247, 228,  88,   5, 184, 179,  69,   6,
  208,  44,  30, 143, 202,  63,  15,   2, 193, 175, 189,   3,   1,  19, 138, 107,
  58, 145,  17,  65,  79, 103, 220, 234, 151, 242, 207, 206, 240, 180, 230, 115,
  150, 172, 116,  34, 231, 173,  53, 133, 226, 249,  55, 232,  28, 117, 223, 110,
  71, 241,  26, 113,  29,  41, 197, 137, 111, 183,  98,  14, 170,  24, 190,  27,
  252,  86,  62,  75, 198, 210, 121,  32, 154, 219, 192, 254, 120, 205,  90, 244,
  31, 221, 168,  51, 136,   7, 199,  49, 177,  18,  16,  89,  39, 128, 236,  95,
  96,  81, 127, 169,  25, 181,  74,  13,  45, 229, 122, 159, 147, 201, 156, 239,
  160, 224,  59,  77, 174,  42, 245, 176, 200, 235, 187,  60, 131,  83, 153,  97,
  23,  43,   4, 126, 186, 119, 214,  38, 225, 105,  20,  99,  85,  33,  12, 125
};



int rcon[10][4] ={
  {1,0,0,0},   // {16#01#,16#00#,16#00#,16#00#},
  {2,0,0,0},   // {16#02#,16#00#,16#00#,16#00#},
  {4,0,0,0},   // {16#04#,16#00#,16#00#,16#00#},
  {8,0,0,0},   // {16#08#,16#00#,16#00#,16#00#},
  {16,0,0,0},  // {16#10#,16#00#,16#00#,16#00#},
  {32,0,0,0},  // {16#20#,16#00#,16#00#,16#00#},
  {64,0,0,0},  // {16#40#,16#00#,16#00#,16#00#},
  {128,0,0,0}, // {16#80#,16#00#,16#00#,16#00#},
  {27,0,0,0},  // {16#1B#,16#00#,16#00#,16#00#},
  {54,0,0,0}   // {16#36#,16#00#,16#00#,16#00#]
};

/* Functions */
int mul(const int a, const int b, const int verbose){
  assert (is_byte(a) && is_byte(b));
  int r = 0;
  if (a != 0 && b != 0){
    r =  Alogtable[(Logtable[a] + Logtable[b]) % 255];
  }

  if (verbose >= 1){ 
    if (verbose >= 2){
      printf("Input: a\n");
      printf("Input: b\n");
      printf("Output: r\n");
      printf("Global: Alogtable\n");
      printf("Global: Logtable\n");
      printf("ExtFun: add\n");
      printf("ExtFun: mod255\n");
    }
    printf("l0: r a b Alogtable Logtable\n");
    printf("l0: [%d] [%d] [%d]", r, a, b);
    print_array(Alogtable, byte, FALSE, FALSE);
    print_array(Logtable, byte, FALSE, FALSE);
  }
  assert(is_byte(r));
  return r;
  }


void word_xor(int r[], const int a[], const int b[], 
	      const int siz, const int verbose){
  assert(is_word(a, siz));
  assert(is_word(b, siz));
  
  r[0] = xor(a[0],b[0]);
  r[1] = xor(a[1],b[1]);
  r[2] = xor(a[2],b[2]);
  r[3] = xor(a[3],b[3]);
    
  /* if (verbose >= 1){ */
  /*   if (verbose >= 2){ */
  /*     print 'Input: a'; */
  /*     print 'Input: b'; */
  /*     print 'Output: r'; */
  /*     print 'ExtFun: xor'; */
  /*     print 'l0: r a b'; */
  /*     print 'l0: ', r, a, b; */
  /*   } */
  /* } */
  assert (is_word(r, siz));

}


void word_xor_xor(int r[word_index], 
		  const int a[word_index], const int b[word_index], const int c[word_index], 
		  const int siz, const int verbose){
  assert(is_word(a, siz));
  assert(is_word(b, siz));
  assert(is_word(c, siz));

  r[0] = xor_xor(a[0],b[0],c[0]);
  r[1] = xor_xor(a[1],b[1],c[1]);
  r[2] = xor_xor(a[2],b[2],c[2]);
  r[2] = xor_xor(a[3],b[3],c[3]);

    /* if verbose >= 1{ #tvn traces */
    /*     if verbose >= 2{ */
    /*         print 'Input: a' */
    /*         print 'Input: b' */
    /*         print 'Input: c' */
    /*         print 'Output: r' */
    /*         print 'ExtFun: xor_xor' */

    /*     print 'l0: r a b c' */
    /*     print 'l0: ', r, a, b, c */

  assert(is_word(r, siz));
}



void SubWord(int r[word_index], const int w[word_index], 
	     const int siz, const int verbose){
  /*
    tvn: *nested* array
    r[i] = S[w[i]]
  */
  
  r[0] = S[w[0]];
  r[1] = S[w[1]];
  r[2] = S[w[2]];
  r[3] = S[w[3]];

    /* if verbose >= 1{    #tvn traces */
    /*     if verbose >= 2{ */
    /*         print 'Input: w' */
    /*         print 'Output: r' */
    /*         print 'Global: Si' */
    /*         print 'Global: S' */
    /*         print 'Global: Alogtable' */
    /*         print 'Global: Logtable' */

    /*     print 'l0: r w Si S Alogtable Logtable' */
    /*     print 'l0: ', r, w, Si, S, Alogtable, Logtable */

  assert(is_word(r, siz));
}


void RotWord(int r[word_index], const int w[word_index], 
	     const int siz, const int verbose){
  /*
    #tvn: miscs array type
    r0 = w1 , r1 = w2,  r2 = w3, r3 = w0
  */

  r[0] = w[1];
  r[1] = w[2];
  r[2] = w[3];
  r[3] = w[0];

    /* //tvn traces */
    /* if verbose >= 1{ */
    /*     print 'l0: r w' */
    /*     print 'l0: ', r,w */

  assert(is_word(r, siz));
}

void Block2State(int r[state_index][word_index], 
		 const int t[byte_index],
		 const int verbose){
  /*
    tvn: multidim array inv
    r[i][j] = t[4i+j]
  */
  assert(is_block(t,byte_index));
  //inv : should get this:  r00 = t0 , r01 = t1  ....
  r[0][0] = t[0];
  r[0][1] = t[1];
  r[0][2] = t[2];
  r[0][3] = t[3];

  r[1][0] = t[4];
  r[1][1] = t[5];
  r[1][2] = t[6];
  r[1][3] = t[7];

  r[2][0] = t[8];
  r[2][1] = t[9]; 
  r[2][2] = t[10];
  r[2][3] = t[11];

  r[3][0] = t[12];
  r[3][1] = t[13]; 
  r[3][2] = t[14];
  r[3][3] = t[15];
    
    /* //tvn traces */
    /* if verbose >= 1{ */
    /*     print 'l0: r t' */
    /*     print 'l0: ', r, t */

    /* assert is_state(r) */
    /* return r */
}

void State2Block(int r[byte_index],
		 const int st[state_index][word_index], 
		 const int verbose){
  /*
    tvn: multidim array inv
    r[4*i+j] = st[i][j]
  */

  //inv: should get this:r0 = st00 ...
  r[0] = st[0][0];
  r[1] = st[0][1];
  r[2] = st[0][2];
  r[3] = st[0][3];
  r[4] = st[1][0];
  r[5] = st[1][1];
  r[6] = st[1][2];
  r[7] = st[1][3];
  r[8] = st[2][0];
  r[9] = st[2][1];
  r[10] = st[2][2];
  r[11] = st[2][3];
  r[12] = st[3][0];
  r[13] = st[3][1];
  r[14] = st[3][2];
  r[15] = st[3][3];


    /* //tvn traces */
    /* if verbose >= 1{ */
    /*     print 'l0: r st' */
    /*     print 'l0: ', r, st */

  assert(is_block(r,byte_index));
}

void SubBytes(int r[state_index][word_index], 
	      const int st[state_index][word_index], 
	      const int verbose){
  /*
    tvn: *nested* array
    r[i,j] = S[st[i,j]]
  */
  r[0][0] = S[st[0][0]];
  r[0][1] = S[st[0][1]]; 
  r[0][2] = S[st[0][2]];
  r[0][3] = S[st[0][3]];


  r[1][0] = S[st[1][0]];
  r[1][1] = S[st[1][1]];
  r[1][2] = S[st[1][2]];
  r[1][3] = S[st[1][3]];

  r[2][0] = S[st[2][0]];
  r[2][1] = S[st[2][1]];
  r[2][2] = S[st[2][2]];
  r[2][3] = S[st[2][3]];

  r[3][0] = S[st[3][0]];
  r[3][1] = S[st[3][1]];
  r[3][2] = S[st[3][2]];
  r[3][3] = S[st[3][3]];

    /* if verbose >= 1{ //tvn traces */
    /*     print 'l0: r st S Si' */
    /*     print 'l0: ', r, st, S, Si */

  //assert is_state(r)

}

void InvSubBytes(int r[state_index][word_index], 
		 const int st[state_index][word_index], 
		 const int verbose){
  /*
    tvn: *nested* array
    r[i,j] = Si[st[i,j]]
  */

  r[0][0] = Si[st[0][0]];
  r[0][1] = Si[st[0][1]]; 
  r[0][2] = Si[st[0][2]];
  r[0][3] = Si[st[0][3]];

  r[1][0] = Si[st[1][0]];
  r[1][1] = Si[st[1][1]];
  r[1][2] = Si[st[1][2]];
  r[1][3] = Si[st[1][3]];

  r[2][0] = Si[st[2][0]];
  r[2][1] = Si[st[2][1]];
  r[2][2] = Si[st[2][2]];
  r[2][3] = Si[st[2][3]];

  r[3][0] = Si[st[3][0]];
  r[3][1] = Si[st[3][1]];
  r[3][2] = Si[st[3][2]];
  r[3][3] = Si[st[3][3]];
    
    /* if verbose >= 1{ //tvn traces */
    /*     print 'l0: r st S Si' */
    /*     print 'l0: ', r, st, S, Si */
    /* 	  } */
  //assert(is_state(r));
}


void ShiftRows(int r[state_index][word_index],
	       const int st[state_index][word_index], 
	       const int verbose){
  /*
    tvn: miscs array inv
    [-r_3_3 + st_2_3 == 0, r_2_0 - st_2_0 == 0, r_3_1 - st_0_1 == 0, -r_1_3 + st_0_3 == 0, -r_2_2 + st_0_2 == 0, r_1_0 - st_1_0 == 0, r_0_1 - st_1_1 == 0, -r_0_3 + st_3_3 == 0, r_3_0 - st_3_0 == 0, r_0_0 - st_0_0 == 0, -r_3_2 + st_1_2 == 0, r_1_1 - st_2_1 == 0, -r_0_2 + st_2_2 == 0, r_2_1 - st_3_1 == 0, -r_2_3 + st_1_3 == 0, r_1_2 - st_3_2 == 0]

    >>> ShiftRows([[1,3,4,255],[1,2,3,4],[7,8,10,52],[0,1,2,15]],verbose=0)
    [[1, 2, 10, 15], [1, 8, 2, 255], [7, 1, 4, 4], [0, 3, 3, 52]]

    >>> ShiftRows([[1,13,4,55],[1,22,3,4],[7,18,10,52],[0,1,2,15]],verbose=0)
    [[1, 22, 10, 15], [1, 18, 2, 55], [7, 1, 4, 4], [0, 13, 3, 52]]
  */


  r[0][0] = st[0][0];
  r[0][1] = st[1][1];
  r[0][2] = st[2][2];
  r[0][3] = st[3][3];  
  r[1][0] = st[1][0];
  r[1][1] = st[2][1];
  r[1][2] = st[3][2];
  r[1][3] = st[0][3];          
  r[2][0] = st[2][0];
  r[2][1] = st[3][1];
  r[2][2] = st[0][2];
  r[2][3] = st[1][3];          
  r[3][0] = st[3][0];
  r[3][1] = st[0][1];
  r[3][2] = st[1][2];
  r[3][3] = st[2][3];

    /* if (verbose>=1){ #tvn traces */
    /*     print 'l0: r st' */
    /*     print 'l0: ', r, st */

    /* assert is_state(r) */

}


void InvShiftRows(int r[state_index][word_index],
		  const int st[state_index][word_index], 
		  const int verbose){
  /*
    tvn: miscs array inv
    [-r_1_3 + st_2_3 == 0, -r_0_2 + st_2_2 == 0, r_3_1 - st_2_1 == 0, -r_3_3 + st_0_3 == 0, -r_2_2 + st_0_2 == 0, -r_2_1 + st_1_1 == 0, r_0_1 - st_3_1 == 0, r_2_0 - st_2_0 == 0, r_3_0 - st_3_0 == 0, r_0_0 - st_0_0 == 0, -r_3_2 + st_1_2 == 0, r_1_1 - st_0_1 == 0, r_1_0 - st_1_0 == 0, r_2_3 - st_3_3 == 0, -r_0_3 + st_1_3 == 0, r_1_2 - st_3_2 == 0]
  */

    /* if (verbose>=1){ #tvn traces */
    /*     print 'l1: st' */
    /*     print 'l1: ', st */

  r[0][0] = st[0][0]; 
  r[0][1] = st[3][1]; 
  r[0][2] = st[2][2]; 
  r[0][3] = st[1][3];
  r[1][0] = st[1][0]; 
  r[1][1] = st[0][1]; 
  r[1][2] = st[3][2]; 
  r[1][3] = st[2][3];
  r[2][0] = st[2][0]; 
  r[2][1] = st[1][1]; 
  r[2][2] = st[0][2]; 
  r[2][3] = st[3][3];
  r[3][0] = st[3][0]; 
  r[3][1] = st[2][1]; 
  r[3][2] = st[1][2]; 
  r[3][3] = st[0][3];


    /* if (verbose>=1){ #tvn traces */
    /*     print 'l0: r st' */
    /*     print 'l0: ', r, st */

    /* 		     assert is_state(r) */

}



void MixColumns(int r[state_index][word_index], const int st[state_index][word_index], const int verbose){

  int t00[] = {mul(2,st[0][0],verbose), mul(3,st[0][1],verbose), st[0][2],                st[0][3]                };
  int t01[] = {st[0][0],                mul(2,st[0][1],verbose), mul(3,st[0][2],verbose), st[0][3]                };
  int t02[] = {st[0][0],                st[0][1],                mul(2,st[0][2],verbose), mul(3,st[0][3],verbose) };
  int t03[] = {mul(3,st[0][0],verbose), st[0][1],st[0][2],       mul(2,st[0][3],verbose)                          };
  int t10[] = {mul(2,st[1][0],verbose), mul(3,st[1][1],verbose), st[1][2],                st[1][3]                };
  int t11[] = {st[1][0],                mul(2,st[1][1],verbose), mul(3,st[1][2],verbose), st[1][3]                };
  int t12[] = {st[1][0],                st[1][1],                mul(2,st[1][2],verbose), mul(3,st[1][3],verbose) };
  int t13[] = {mul(3,st[1][0],verbose), st[1][1],                st[1][2],                mul(2,st[1][3],verbose) };
  int t20[] = {mul(2,st[2][0],verbose), mul(3,st[2][1],verbose), st[2][2],                st[2][3]                };
  int t21[] = {st[2][0],                mul(2,st[2][1],verbose), mul(3,st[2][2],verbose), st[2][3]                };
  int t22[] = {st[2][0],                st[2][1],                mul(2,st[2][2],verbose), mul(3,st[2][3],verbose)  };
  int t23[] = {mul(3,st[2][0],verbose), st[2][1],                st[2][2],                mul(2,st[2][3],verbose) };
  int t30[] = {mul(2,st[3][0],verbose), mul(3,st[3][1],verbose), st[3][2],                st[3][3]                };
  int t31[] = {st[3][0],                mul(2,st[3][1],verbose), mul(3,st[3][2],verbose), st[3][3]                };
  int t32[] = {st[3][0],                st[3][1],                mul(2,st[3][2],verbose), mul(3,st[3][3],verbose) };
  int t33[] = {mul(3,st[3][0],verbose), st[3][1],                st[3][2],                mul(2,st[3][3],verbose) };


  r[0][0] =  myxor(t00, word_index);
  r[0][1] =  myxor(t01, word_index);
  r[0][2] =  myxor(t02, word_index);
  r[0][3] =  myxor(t03, word_index);
  r[1][0] =  myxor(t10, word_index);
  r[1][1] =  myxor(t11, word_index);
  r[1][2] =  myxor(t12, word_index);
  r[1][3] =  myxor(t13, word_index);
  r[2][0] =  myxor(t20, word_index);
  r[2][1] =  myxor(t21, word_index);
  r[2][2] =  myxor(t22, word_index);
  r[2][3] =  myxor(t23, word_index);
  r[3][0] =  myxor(t30, word_index);
  r[3][1] =  myxor(t31, word_index);
  r[3][2] =  myxor(t32, word_index);
  r[3][3] =  myxor(t33, word_index);

      //assert(is_state(r));

}


void InvMixColumns(int r[state_index][word_index], 
		   const int st[state_index][word_index], 
		   const int verbose){

  int t00[] = {mul(14,st[0][0],verbose), mul(11,st[0][1],verbose), mul(13,st[0][2],verbose), mul( 9,st[0][3], verbose)};
  int t01[] = {mul( 9,st[0][0],verbose), mul(14,st[0][1],verbose), mul(11,st[0][2],verbose), mul(13,st[0][3], verbose)};
  int t02[] = {mul(13,st[0][0],verbose), mul( 9,st[0][1],verbose), mul(14,st[0][2],verbose), mul(11,st[0][3], verbose)};
  int t03[] = {mul(11,st[0][0],verbose), mul(13,st[0][1],verbose), mul( 9,st[0][2],verbose), mul(14,st[0][3], verbose)};
  
  int t10[] = {mul(14,st[1][0],verbose), mul(11,st[1][1],verbose), mul(13,st[1][2],verbose), mul( 9,st[1][3], verbose)};
  int t11[] = {mul( 9,st[1][0],verbose), mul(14,st[1][1],verbose), mul(11,st[1][2],verbose), mul(13,st[1][3], verbose)};
  int t12[] = {mul(13,st[1][0],verbose), mul( 9,st[1][1],verbose), mul(14,st[1][2],verbose), mul(11,st[1][3], verbose)};
  int t13[] = {mul(11,st[1][0],verbose), mul(13,st[1][1],verbose), mul( 9,st[1][2],verbose), mul(14,st[1][3], verbose)};
  
  int t20[] = {mul(14,st[2][0],verbose), mul(11,st[2][1],verbose), mul(13,st[2][2],verbose), mul( 9,st[2][3], verbose)};
  int t21[] = {mul( 9,st[2][0],verbose), mul(14,st[2][1],verbose), mul(11,st[2][2],verbose), mul(13,st[2][3], verbose)};
  int t22[] = {mul(13,st[2][0],verbose), mul( 9,st[2][1],verbose), mul(14,st[2][2],verbose), mul(11,st[2][3], verbose)};
  int t23[] = {mul(11,st[2][0],verbose), mul(13,st[2][1],verbose), mul( 9,st[2][2],verbose), mul(14,st[2][3], verbose)};
  
  int t30[] = {mul(14,st[3][0],verbose), mul(11,st[3][1],verbose), mul(13,st[3][2],verbose), mul( 9,st[3][3], verbose)};
  int t31[] = {mul( 9,st[3][0],verbose), mul(14,st[3][1],verbose), mul(11,st[3][2],verbose), mul(13,st[3][3], verbose)};
  int t32[] = {mul(13,st[3][0],verbose), mul( 9,st[3][1],verbose), mul(14,st[3][2],verbose), mul(11,st[3][3], verbose)};
  int t33[] = {mul(11,st[3][0],verbose), mul(13,st[3][1],verbose), mul( 9,st[3][2],verbose), mul(14,st[3][3], verbose)};
  
  
  r[0][0] =  myxor(t00, word_index);
  r[0][1] =  myxor(t01, word_index);
  r[0][2] =  myxor(t02, word_index);
  r[0][3] =  myxor(t03, word_index);
  r[1][0] =  myxor(t10, word_index);
  r[1][1] =  myxor(t11, word_index);
  r[1][2] =  myxor(t12, word_index);
  r[1][3] =  myxor(t13, word_index);
  r[2][0] =  myxor(t20, word_index);
  r[2][1] =  myxor(t21, word_index);
  r[2][2] =  myxor(t22, word_index);
  r[2][3] =  myxor(t23, word_index);
  r[3][0] =  myxor(t30, word_index);
  r[3][1] =  myxor(t31, word_index);
  r[3][2] =  myxor(t32, word_index);
  r[3][3] =  myxor(t33, word_index);

	 //assert is_state(r)
}


void AddRoundKey(int r[state_index][word_index],
		 const int st[state_index][word_index], 
		 const int rk0[word_index], 
		 const int rk1[word_index], 
		 const int rk2[word_index], 
		 const int rk3[word_index], 
		 const int verbose){
  /*
    *nested* array
    r[i][j] = xor(st[i][j], rk[i][j])
  */
    /* if verbose >= 2: */
    /*     print 'Input: st' */
    /*     print 'Input: rk0' */
    /*     print 'Input: rk1' */
    /*     print 'Input: rk2' */
    /*     print 'Input: rk3' */
    /*     print 'Output: r_' */
    /*     print 'ExtFun: xor' */

    /* if verbose >= 1: */
    /*     print 'l1: st rk0 rk1 rk2 rk3' */
    /*     print 'l1: ', st, rk0, rk1, rk2, rk3 */

  r[0][0] = xor(st[0][0], rk0[0]);
  r[0][1] = xor(st[0][1], rk0[1]);
  r[0][2] = xor(st[0][2], rk0[2]);
  r[0][3] = xor(st[0][3], rk0[3]);
  r[1][0] = xor(st[1][0], rk1[0]);
  r[1][1] = xor(st[1][1], rk1[1]);
  r[1][2] = xor(st[1][2], rk1[2]);
  r[1][3] = xor(st[1][3], rk1[3]);
  r[2][0] = xor(st[2][0], rk2[0]);
  r[2][1] = xor(st[2][1], rk2[1]);
  r[2][2] = xor(st[2][2], rk2[2]);
  r[2][3] = xor(st[2][3], rk2[3]);
  r[3][0] = xor(st[3][0], rk3[0]);
  r[3][1] = xor(st[3][1], rk3[1]);
  r[3][2] = xor(st[3][2], rk3[2]);
  r[3][3] = xor(st[3][3], rk3[3]);

    /* if verbose >= 1: */
    /*     print 'l0: st rk0 rk1 rk2 rk3 r_' */
    /*     print 'l0: ', st, rk0, rk1, rk2, rk3, r */


    /* assert is_state(r) */

}

    /*
void AddRoundKey_vn(st, rk, const int verbose){
    assert is_state(st)
    assert is_word(rk[0]) and is_word(rk[1]) and is_word(rk[2]) and is_word(rk[3])

    if verbose >= 2:
        print 'Input: st'
        print 'Input: rk'
        print 'Output: r_'
        print 'ExtFun: xor'

    if verbose >= 1:
        print 'l1: st rk'
        print 'l1: ', st, rk

    r = [
        [xor(st[0][0], rk[0][0]), xor(st[0][1], rk[0][1]), xor(st[0][2], rk[0][2]), xor(st[0][3], rk[0][3])],
        [xor(st[1][0], rk[1][0]), xor(st[1][1], rk[1][1]), xor(st[1][2], rk[1][2]), xor(st[1][3], rk[1][3])],
        [xor(st[2][0], rk[2][0]), xor(st[2][1], rk[2][1]), xor(st[2][2], rk[2][2]), xor(st[2][3], rk[2][3])],
        [xor(st[3][0], rk[3][0]), xor(st[3][1], rk[3][1]), xor(st[3][2], rk[3][2]), xor(st[3][3], rk[3][3])]
        ]


    if verbose >= 1:
        print 'l0: st rk r_'
        print 'l0: ', st, rk, r

    assert is_state(r)
    }



void AddRoundKey_vn_simple(st,rk, const int verbose){
    """
    for debugging purpose
    """

    st = [st_[:3] for st_ in st[:3]]
    rk = [rk_[:3] for rk_ in rk[:3]]

    if verbose >= 2:
        print 'Input: st'
        print 'Input: rk'
        print 'Output: r_'
        print 'ExtFun: xor'

    if verbose >= 1:
        print 'l1: st rk'
        print 'l1: ', st, rk

    r = [
        [xor(st[0][0], rk[0][0]), xor(st[0][1], rk[0][1]), xor(st[0][2], rk[0][2])],
        [xor(st[1][0], rk[1][0]), xor(st[1][1], rk[1][1]), xor(st[1][2], rk[1][2])],
        [xor(st[2][0], rk[2][0]), xor(st[2][1], rk[2][1]), xor(st[2][2], rk[2][2])]
        ]

    if verbose >= 1:
        print 'l0: st rk r_'
        print 'l0: ', st, rk, r


    }
    */

void init_2d_arr(int arr[][word_index], const int siz){
  int i = 0, j = 0 ;
  for(; i < siz; ++i){
    for (; j < word_index; ++j){
      arr[i][j] = 0;
    }
  }
}
void init_rk(int rk[roundkey_index][word_index]){  
  /*  create an array of roundkey_index empty words
      rk = [[0,0,0,0]]*roundkey_index  */
  init_2d_arr(rk,roundkey_index);
}

void KeySetupEnc4(int rk[roundkey_index][word_index],
		  const int cipherKey[key_index], 
		  const int verbose){
    /*
      return key_schedule
      tvn: *multidim* array
      rk[i][j] = cipherKey[4*i+j]
    */
  const int k = 4;
  int i = 0;
  int j = 0;

  init_rk(rk);

  for(i=0; i < k; ++i){
    for (j=0; j < word_index; ++j){
      rk[i][j] = cipherKey[4*i +j];
    }
  }

    /* # #tvn: traces */
    /* # if verbose >= 1: */
    /* #     print 'rk cipherKey' */
    /* #     print rk[:4], cipherKey */



    /* #--# assert 0 <= i and i <= 3 and */
    /* #--#        (for all p in Integer range 0 .. i => */
    /* #--#          (rk(p) = [cipherKey(4*p), cipherKey(4*p+1), cipherKey(4*p+2), cipherKey(4*p+3)))) */
  
  int w[] = {0,0,0,0};
  int rw[] = {0,0,0,0};
  int sw[] = {0,0,0,0};
  for(i = k; i < 44; ++i){// in range(4,44){
    if(i % k == 0){
      RotWord(rw, rk[i-1], word_index, verbose);
      SubWord(sw, rw, word_index,verbose);
      word_xor_xor(w, rk[i-k], sw, rcon[i/k-1], word_index, verbose);
      for(j=0; j < word_index; ++j){
	rk[i][j] = w[j];
      }
    }
    else{
      word_xor(w,rk[i-k],rk[i-1],word_index,verbose);
      for(j=0; j < word_index; ++j){
	rk[i][j] = w[j];
      }
    }
  }

    /* #tvn: traces */
    /* # if verbose >= 1: */
    /* #     print 'rk_else cipherKey' */
    /* #     print [rk[i] for i in range(4,44)], cipherKey */

    /* # assert 4 <= i and i <= 43 and */
    /* # (for all p in Integer range 0 .. 3 => */
    /* #          (rk(p) = [cipherKey(4*p), cipherKey(4*p+1), cipherKey(4*p+2), cipherKey(4*p+3)))) and */
    /* #        (for all p in Integer range 4 .. i => */
    /* #          ((p mod 4 = 0 and rk(p) = word_xor_xor(rk(p-4), SubWord(RotWord(rk(p-1))), rcon(p/4-1))) or */
    /* #          (p mod 4 /= 0 and rk(p) = word_xor(rk(p-4), rk(p-1))))) */


    /* #disj */
    /* if verbose >= 1: */
    /*     print 'l0: rk cipherKey' */
    /*     print 'l0: ', rk, cipherKey */


    /* assert is_key_schedule(rk) */

}


void KeySetupEnc6(int rk[roundkey_index][word_index],
		  const int cipherKey[key_index], 
		  const int verbose){
  /*
    tvn: *multidim* array
    rk[i][j] = cipherKey[4*i+j]
  */

  assert(is_key(cipherKey,key_index));

  const int k = 6;
  int i = 0;
  int j = 0;

  init_rk(rk);

  for(i=0; i < k; ++i){
    for (j=0; j < word_index; ++j){
      rk[i][j] = cipherKey[4*i +j];
    }
  }

    /* #tvn: traces */
    /* # if verbose >= 1: */
    /* #     print 'rk cipherKey' */
    /* #     print rk[:6], cipherKey */

    /*     # assert 0 <= i and i <= 5 and */
    /*     #        (for all p in Integer range 0 .. i => */
    /*     #          (rk(p) = [cipherKey(4*p), cipherKey(4*p+1), cipherKey(4*p+2), cipherKey(4*p+3)))) */


  int w[] = {0,0,0,0};
  int rw[] = {0,0,0,0};
  int sw[] = {0,0,0,0};
  for(i=k; i < 52; ++i){// in range(6,52):
    if (i % k == 0){
      RotWord(rw, rk[i-1], word_index, verbose);
      SubWord(sw, rw, word_index,verbose);
      word_xor_xor(w, rk[i-k], sw, rcon[i/k-1], word_index, verbose);
      for(j=0; j < word_index; ++j){
	rk[i][j] = w[j];
      }
    }
    else{
      word_xor(w,rk[i-k],rk[i-1],word_index,verbose);
      for(j=0; j < word_index; ++j){
	rk[i][j] = w[j];
      }
    }
  }
    /* #tvn: traces */
    /* if verbose >= 1: */
    /*     print 'l0: rk cipherKey' */
    /*     print 'l0: ', rk, cipherKey */


        /* # assert 6 <= i and i <= 51 and */
        /* #        (for all p in Integer range 0 .. 5 => */
        /* #          (rk(p) = [cipherKey(4*p), cipherKey(4*p+1), cipherKey(4*p+2), cipherKey(4*p+3)))) and */
        /* #        (for all p in Integer range 6 .. i => */
        /* #          ((p mod 6 = 0 and rk(p) = word_xor_xor(rk(p-6), SubWord(RotWord(rk(p-1))), rcon(p/6-1))) or */
        /* #          (p mod 6 /= 0 and rk(p) = word_xor(rk(p-6), rk(p-1))))) */

    /* assert is_key_schedule(rk) */

}

void KeySetupEnc8(int rk[roundkey_index][word_index],
		  const int cipherKey[key_index], 
		  const int verbose){
  /*
    tvn: *multidim* array
    rk[i][j] = cipherKey[4*i+j]
  */
  
  const int k = 8;
  int i = 0;
  int j = 0;

  init_rk(rk);

  for(i=0; i < k; ++i){
    for (j=0; j < word_index; ++j){
      rk[i][j] = cipherKey[4*i +j];
    }
  }

    /* #tvn: traces */
    /* if verbose >= 1: */
    /*     print 'rk cipherKey' */
    /*     print rk[:8], cipherKey */

    /*     # assert 0 <= i and i <= 7 and */
    /*     #        (for all p in Integer range 0 .. i => */
    /*     #          (rk(p) = [cipherKey(4*p), cipherKey(4*p+1), cipherKey(4*p+2), cipherKey(4*p+3)))) */


  int w[] = {0,0,0,0};
  int rw[] = {0,0,0,0};
  int sw[] = {0,0,0,0};
  for(i = k; i < 60; ++i){// in range(8,60){
    if(i % k == 0){
      RotWord(rw, rk[i-1], word_index, verbose);
      SubWord(sw, rw, word_index,verbose);
      word_xor_xor(w, rk[i-k], sw, rcon[i/k-1], word_index, verbose);
      for(j=0; j < word_index; ++j){
	rk[i][j] = w[j];
      }
    }
    else if(i % 4 == 0){
      SubWord(sw, rk[i-1], word_index, verbose);
      word_xor(w,rk[i-k],sw,word_index,verbose);
      for(j=0; j < word_index; ++j){
	rk[i][j] = w[j];
      }
      
    }
    else{
      word_xor(w,rk[i-k],rk[i-1],word_index,verbose);
      for(j=0; j < word_index; ++j){
	rk[i][j] = w[j];
      }
    }
  }

    /* for i in range(8,60): */
    /*     if (i % 8 == 0): */
    /*         rk[i] = word_xor_xor(rk[i-8], SubWord(RotWord(rk[i-1],verbose=0),verbose=0), */
    /*                              rcon[i/8-1],verbose=0) */
    /*     elif (i % 4 == 0): */
    /*         rk[i] = word_xor(rk[i-8], SubWord(rk[i-1],verbose=0),verbose=0) */
    /*     else: */
    /*         rk[i] = word_xor(rk[i-8], rk[i-1],verbose=0) */

    /*     # assert 8 <= i and i <= 59 and */
    /*     #        (for all p in Integer range 0 .. 7 => */
    /*     #          (rk(p) = [cipherKey(4*p), cipherKey(4*p+1), cipherKey(4*p+2), cipherKey(4*p+3)))) and */
    /*     #        (for all p in Integer range 8 .. i => */
    /*     #          ((p % 8 = 0 and                  rk(p) = word_xor_xor(rk(p-8), SubWord(RotWord(rk(p-1))), rcon(p/8-1))) or */
    /*     #          (p % 8 /= 0 and p % 4 = 0 and  rk(p) = word_xor(rk(p-8), SubWord(rk(p-1)))) or */
    /*     #          (p % 8 /= 0 and p % 4 /= 0 and rk(p) = word_xor(rk(p-8), rk(p-1))))) */


    /* assert is_key_schedule(rk) */
}


void KeySetupEnc(int rk[roundkey_index][word_index],
		 const int cipherKey[key_index], 
		 const int Nk, 
		 const int verbose){ //return key_schedule

  assert(is_key(cipherKey, key_index));

  init_rk(rk);

  if (Nk == 4){
    KeySetupEnc4(rk,cipherKey,verbose);
  }
  if (Nk == 6){
    KeySetupEnc6(rk,cipherKey,verbose);
  }
  if (Nk == 8){
    KeySetupEnc8(rk,cipherKey,verbose);
  }

  /* rk = [[0,0,0,0]]*roundkey_index  #create an array of roundkey_index empty words */
  /*   if (Nk == 4): */
  /*       rk = KeySetupEnc4(cipherKey,verbose) */
  /*   if (Nk == 6): */
  /*       rk = KeySetupEnc6(cipherKey,verbose) */
  /*   if (Nk == 8): */
  /*       rk = KeySetupEnc8(cipherKey,verbose) */
  /* 	  //assert is_key_schedule(rk) */
  /* 	  } */
}

/*
void KeyScheduleMod1__(W, Nr,const int verbose){ #return key_schedule
    assert is_key_schedule(W)
    rk = deepcopy(W)
    for i in range(4*(Nr+1)):
        #print i, 4*(Nr-i//4)+i%4
        #print i, (4*Nr- 4*(i//4))+i%4
        rk[i] = W[4*(Nr-i//4)+i%4]

        #rk[i] = W[add(mul4(sub(Nr,div4(i))),mod4(i))]

        # assert 0 <= i and i <= Nr and Nr = Nr% and
        #        (for all p in Integer range 0 .. i =>
        #          (rk(4*p  ) = W(4*(Nr-p)  ) and
        #           rk(4*p+1) = W(4*(Nr-p)+1) and
        #           rk(4*p+2) = W(4*(Nr-p)+2) and
        #           rk(4*p+3) = W(4*(Nr-p)+3))) and
        #        (for all j in Integer range 4*(Nr+1) .. 4*(MAXNR+1)-1 =>
        #          (rk(j) = W(j)))

    if verbose >= 1:
        # vs1 = ['RK_%d_%d'%(i,j) for i in range(len(rk)) for j in range(len(rk[0]))]
        # ns1 = [rk[i][j]  for i in range(len(rk)) for j in range(len(rk[0]))]

        # vs2 = ['W_%d_%d'%(i,j) for i in range(len(W)) for j in range(len(W[0]))]
        # ns2 = [W[i][j]  for i in range(len(W)) for j in range(len(W[0]))]


        # print ' '.join(vs1+vs2)
        # print ' '.join(map(str,ns1+ns2))
        print 'l0: rk, W, Nr'
        print 'l0: ', rk, W, [Nr]

    assert is_key_schedule(rk)
    return rk


void KeyScheduleMod1_(W, Nr,const int verbose){ #return key_schedule
    assert is_key_schedule(W)
    rk = deepcopy(W)
    for i in range(Nr+1):
        for j in range(len(rk[0])):
            rk[4*i+j] =W[4*(Nr-i)+j]
        # assert 0 <= i and i <= Nr and Nr = Nr% and
        #        (for all p in Integer range 0 .. i =>
        #          (rk(4*p  ) = W(4*(Nr-p)  ) and
        #           rk(4*p+1) = W(4*(Nr-p)+1) and
        #           rk(4*p+2) = W(4*(Nr-p)+2) and
        #           rk(4*p+3) = W(4*(Nr-p)+3))) and
        #        (for all j in Integer range 4*(Nr+1) .. 4*(MAXNR+1)-1 =>
        #          (rk(j) = W(j)))

    if verbose >= 1:
        vs1 = ['RK_%d_%d'%(i,j) for i in range(len(rk)) for j in range(len(rk[0]))]
        ns1 = [rk[i][j]  for i in range(len(rk)) for j in range(len(rk[0]))]

        vs2 = ['W_%d_%d'%(i,j) for i in range(len(W)) for j in range(len(W[0]))]
        ns2 = [W[i][j]  for i in range(len(W)) for j in range(len(W[0]))]


        print 'l0: ', ' '.join(vs1+vs2)
        print 'l0: ', ' '.join(map(str,ns1+ns2))

    assert is_key_schedule(rk)
    return rk
*/



void copy_rk(int rk[roundkey_index][word_index],
	     const int W[roundkey_index][word_index]){  
  /*  copy W to rk
      rk = [[0,0,0,0]]*roundkey_index  
      rk = deepcopy(W)
  */

  int i = 0, j = 0 ;
  for(; i < roundkey_index; ++i){
    for (; j < word_index; ++j){
      rk[i][j] = W[i][j];
    }
  }
}

void KeyScheduleMod1(int rk[roundkey_index][word_index],
		     const int W[roundkey_index][word_index], 
		     const int Nr,
		     const int verbose){

  //assert is_key_schedule(W)
  copy_rk(rk, W);
  int i = 0;
  int j = 0;
  int k = 0;
  for(i=0; i < Nr+1 ; ++i){
    for(k = 0; k < 4; ++k){
      for(j=0; j < word_index; ++j){
	rk[4*i+k][j]   = W[4*(Nr-i)+k][j];
      }
    }
  }

        /* # assert 0 <= i and i <= Nr and Nr = Nr% and */
        /* #        (for all p in Integer range 0 .. i => */
        /* #          (rk(4*p  ) = W(4*(Nr-p)  ) and */
        /* #           rk(4*p+1) = W(4*(Nr-p)+1) and */
        /* #           rk(4*p+2) = W(4*(Nr-p)+2) and */
        /* #           rk(4*p+3) = W(4*(Nr-p)+3))) and */
        /* #        (for all j in Integer range 4*(Nr+1) .. 4*(MAXNR+1)-1 => */
        /* #          (rk(j) = W(j))) */

    /* if verbose >= 1: */
    /*     vs1 = ['RK_%d_%d'%(i,j) for i in range(len(rk)) for j in range(len(rk[0]))] */
    /*     ns1 = [rk[i][j]  for i in range(len(rk)) for j in range(len(rk[0]))] */

    /*     vs2 = ['W_%d_%d'%(i,j) for i in range(len(W)) for j in range(len(W[0]))] */
    /*     ns2 = [W[i][j]  for i in range(len(W)) for j in range(len(W[0]))] */


    /*     print 'l0: ', ' '.join(vs1+vs2) */
    /*     print 'l0: ', ' '.join(map(str,ns1+ns2)) */

    /* assert is_key_schedule(rk) */
    /* return rk */
}


void KeyScheduleMod2(int rk[roundkey_index][word_index],
		     const int W[roundkey_index][word_index], 
		     const int Nr,
		     const int verbose){
  //assert is_key_schedule(W)
  int st[state_index][word_index];
  int st_[state_index][word_index];

  copy_rk(rk, W);
  int i = 0;
  int j = 0;
  int k = 0;
  //for i in range(1,Nr){
  for (i = 1; i < Nr; ++i){

    /* st[0] = W[4*i]; */
    /* st[1] = W[4*i+1]; */
    /* st[2] = W[4*i+2]; */
    /* st[3] = W[4*i+3]]; */

    for (k=0; k < 4; ++k){
      for (j=0; j < word_index; ++j){
	st_[k][j] = W[4*i+k][j];
      }
    }

    InvMixColumns(st, st_, verbose);
    /* rk[4*i]   = st[0] */
    /* rk[4*i+1] = st[1] */
    /* rk[4*i+2] = st[2] */
    /* rk[4*i+3] = st[3] */

    for (k=0; k < 4; ++k){
      for (j=0; j < word_index; ++j){
	rk[4*i+k][j] = st[k][j];
      }
    }
  }

        /* # assert 1 <= i and i <= Nr-1 and Nr = Nr% and */
        /* #        (for all p in Integer range 1 .. i => */
        /* #          (rk(4*p  ) = InvMixColumns([W(4*p), W(4*p+1), W(4*p+2), W(4*p+3)))(0) and */
        /* #           rk(4*p+1) = InvMixColumns([W(4*p), W(4*p+1), W(4*p+2), W(4*p+3)))(1) and */
        /* #           rk(4*p+2) = InvMixColumns([W(4*p), W(4*p+1), W(4*p+2), W(4*p+3)))(2) and */
        /* #           rk(4*p+3) = InvMixColumns([W(4*p), W(4*p+1), W(4*p+2), W(4*p+3)))(3))) and */
        /* #        (for all j in Integer range 0 .. 3 => */
        /* #          (rk(j) = W(j))) and */
        /* #        (for all j in Integer range 4*Nr .. 4*(MAXNR+1)-1 => */
        /* #          (rk(j) = W(j))) */


}	


void KeySetupDec(int rk[roundkey_index][word_index],
		 const int cipherKey[key_index], 
		 const int Nk, 
		 const int verbose){
  
  assert(is_key(cipherKey, key_index));
  
  int kse[roundkey_index][word_index];
  int ksm1[roundkey_index][word_index];
  KeySetupEnc(kse,cipherKey,Nk,verbose);
  KeyScheduleMod1(ksm1,kse,Nk+6,verbose);

  KeyScheduleMod2(rk,ksm1,Nk+6,verbose);

    //assert is_key_schedule(r)
}


int AesKeySetupEnc(int rk[roundkey_index][word_index],
		    const int cipherKey[key_index], 
		    const int keyBits, 
		    const int verbose){

  assert(is_key(cipherKey, key_index));
  int Nr = keyBits/32 + 6;
  KeySetupEnc(rk, cipherKey, keyBits/32,verbose);

    //    assert is_key_schedule(rk)

    /* if verbose >= 1: #tvntraces */
    /*     print 'l0: Nr keyBits' */
    /*     print 'l0: ', Nr, keyBits */

    /* 	       return Nr,rk */
  return Nr;
}



int AesKeySetupDec(int rk[roundkey_index][word_index],
		    const int cipherKey[key_index], 
		    const int keyBits, 
		    const int verbose){

  assert(is_key(cipherKey, key_index));
  int Nr = keyBits/32 + 6;
  KeySetupDec(rk, cipherKey, keyBits/32,verbose);
  
  /* assert is_key_schedule(rk) */

  /*   if verbose >= 1: #tvntraces */
  /*       print 'l0: Nr keyBits' */
  /*       print 'l0: ', Nr, keyBits */

  return Nr;
}

/*

void AesEncrypt(rk,Nr,pt,const int verbose){
  //assert is_key_schedule(rk)
  assert(is_block(pt,byte_index));
  int r[state_index][word_index];
  int st[state_index][word_index];
  b2s = Block2State(r,pt,verbose);
  AddRoundKey(st, b2s, rk[0], rk[1], rk[2], rk[3], verbose);
  //assert st = encrypt_round(Block2State(pt), rk, Nr, 0)

    for r in range(1,Nr):

        sb = SubBytes(st,verbose)
        sr = ShiftRows(sb,verbose)

        st = AddRoundKey(MixColumns(sr,verbose), rk[4*r], rk[4*r+1], rk[4*r+2], rk[4*r+3],verbose)


        # assert 1 <= r and r <= Nr-1 and Nr = Nr% and
        #        st = encrypt_round(Block2State(pt), rk, Nr, r)


    st =AddRoundKey(ShiftRows(SubBytes(st,verbose),verbose), rk[4*Nr], rk[4*Nr+1], rk[4*Nr+2], rk[4*Nr+3],verbose)
    # assert st = encrypt_round(Block2State(pt), rk, Nr, Nr)

	  ct =State2Block(st,verbose);
    assert(is_block(ct,block_index));
    return ct



void AesDecrypt(rk, Nr, ct,const int verbose){
    assert is_key_schedule(rk)
    assert is_block(ct)
    st = AddRoundKey(Block2State(ct,verbose), rk[0], rk[1], rk[2], rk[3],verbose)
    # assert st = decrypt_round(Block2State(ct), rk, Nr, 0)


    for r in range(1,Nr):
        st =AddRoundKey(InvMixColumns(InvShiftRows(InvSubBytes(st,verbose),verbose),verbose), rk[4*r], rk[4*r+1], rk[4*r+2], rk[4*r+3],verbose)
        # assert 1 <= r and r <= Nr-1 and Nr = Nr% and
        #        st = decrypt_round(Block2State(ct), rk, Nr, r)


    st =AddRoundKey(InvShiftRows(InvSubBytes(st,verbose),verbose), rk[4*Nr], rk[4*Nr+1], rk[4*Nr+2], rk[4*Nr+3],verbose)
    # assert st = decrypt_round(Block2State(ct), rk, Nr, Nr)

    pt =State2Block(st,verbose)
    assert is_block(pt)
    return pt
*/


void unittest(){
  assert(xor(2,3) == 1);
  assert(xor(8,5) == xor(5,8) && xor(5,8) == 13) ;

  int arr[4] = {3,7,15,9};
  assert(myxor(arr, 4) == 2);
  arr[0] = 3; arr[1] = -7; arr[2] = 5; arr[3]=99;
  assert(myxor(arr, 4) == -100);

  int arr1[2] = {3,7};
  assert(myand(arr1,2) == 3);
}

int main(int argc, char **argv){
  unittest();

  return 0;
}
